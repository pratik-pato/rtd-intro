.. Your Project Title
=====================

Welcome to the documentation for Your Project!

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   introduction
   installation
   usage
   faq
   contributing
   changelog

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
